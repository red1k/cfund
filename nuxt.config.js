require('dotenv').config()

const polyfills = [
  'Promise',
  'Object.assign',
  'Object.values',
  'Array.prototype.find',
  'Array.prototype.findIndex',
  'Array.prototype.includes',
  'String.prototype.includes',
  'String.prototype.startsWith',
  'String.prototype.endsWith'
]

module.exports = {
  // mode: 'spa',

  srcDir: __dirname,

  env: {
    apiUrl: process.env.APP_URL || 'http://api.silison.localhost',
    appName: process.env.APP_NAME || 'Silicon',
    appLocale: process.env.APP_LOCALE || 'mn',
    githubAuth: !!process.env.GITHUB_CLIENT_ID
  },

  head: {
    title: process.env.APP_NAME,
    titleTemplate: '%s - ' + process.env.APP_NAME,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic' },
      { rel: 'stylesheet', href: '//fonts.googleapis.com/icon?family=Material+Icons' },

    // <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"></link>
    ],
    script: [
      { src: `https://cdn.polyfill.io/v2/polyfill.min.js?features=${polyfills.join(',')}` }
    ]
  },

  loading: { color: '#007bff' },

  router: {
    middleware: ['locale', 'check-auth']
  },

  css: [
    'vuetify/dist/vuetify.min.css',
    { src: '~assets/sass/app.scss', lang: 'scss' },
  ],

  plugins: [
    '~components/global',
    '~plugins/i18n',
    '~plugins/vform',
    '~plugins/axios',
    '~plugins/fontawesome',
    // '~plugins/nuxt-client-init',
    { src: '~plugins/bootstrap', ssr: false },
    { src: '~plugins/vuetify', ssr: true },
    { src: '~plugins/directives', ssr: false }
  ],

  modules: [
    '@nuxtjs/router',
    '~/modules/spa'
  ],

  axios: {
    proxyHeaders: false,
    credentials: false
  },

  build: {
    extractCSS: true
  }
}
